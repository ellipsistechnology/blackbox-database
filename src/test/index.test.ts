import { database, configureDatabase, dbGetList, dbDelete, dbUpdate, dbCreate, dbGet, dbGetBy, dbGetListBy, dbGetListWithQuery } from "..";

const method1Value = 'method1 value';

beforeAll(() => {
  configureDatabase({
    url: (<any>global).__MONGO_URI__,
    dbname: (<any>global).__MONGO_DB_NAME__
  })
})

@database()
class ThingDatabase {
  item:string

  method1() {
    return method1Value
  }

  getThing(_id:string) {
    throw new Error(`This should never be called.`)
  }
  getThingList() {}
  createThing(_thing: any) {}
  updateThing(_name: string, _thing: any) {}
  deleteThing(_name: string) {}

  getThingByTest2(_testValue: number) {}
  getThingListByTest2(_testValue: number) {throw new Error('getThingListByTest2() placeholder called')}

  getThingListWithQuery(_q: any): any[] {return []}
}

test("Class decorator generated CRUD", async () => {
  const db = new ThingDatabase()
  const testThing = {
    name: "blah",
    test: "class test: blah blah",
    test2: 1
  }
  const testThing2 = {
    name: "blah2",
    test: "class test: blah blah",
    test2: 1
  }
  const testThing3 = {
    name: "blah3",
    test: "class test: blah blah",
    test2: 2
  }
  const updatedTestThing = {
    name: "blah",
    test: "class test: abc",
    test2: 22
  }

  expect(await db.method1()).toEqual(method1Value)
  expect(await db.getThing('blah')).toBeFalsy()

  await db.createThing(testThing)
  expect(await db.getThing('blah')).toEqual(testThing)

  await db.createThing(testThing2)
  await db.createThing(testThing3)
  const list = await db.getThingList()
  expect((<any>list).length).toBeDefined()
  expect((<any>list).length).toEqual(3)
  expect(list).toEqual(expect.arrayContaining([testThing, testThing2, testThing3]))

  const list2 = await db.getThingListByTest2(1)
  expect((<any>list2).length).toBeDefined()
  expect((<any>list2).length).toEqual(2)
  expect(list2).toEqual(expect.arrayContaining([testThing, testThing2]))

  expect(await db.getThingByTest2(2)).toEqual(testThing3)

  const list3 = await db.getThingListWithQuery({name: "blah2"})
  expect(list3).toBeDefined()
  expect(list3.length).toEqual(1)
  expect(list3).toEqual(expect.arrayContaining([testThing2]))

  await db.updateThing('blah', updatedTestThing)
  expect(await db.getThing('blah')).toEqual(expect.objectContaining(updatedTestThing))

  await db.deleteThing('blah')
  expect(await db.getThing('blah')).toBeFalsy()
})

class BlahDatabase {
  @dbGet()              g(_id:string) {throw new Error(`called dbGet original method`)}
  @dbGetBy('test2')     g2(_val:string) {throw new Error(`called dbGet original method`)}
  @dbGetList()          gl() {}
  @dbGetListBy('test2') gl2(_val: number) {}
  @dbCreate()           c(_thing:any) {throw new Error(`called dbCreate original method`)}
  @dbUpdate()           u(_name:string, _thing:any) {}
  @dbDelete()           d(_name:string) {}
  @dbGetListWithQuery() gwq(_q:any): any[] {return []}
}

test("Method decorator generated CRUD.", async () => {
  const db = new BlahDatabase()

  const testThing = {
    name: "blah",
    test: "method test: blah blah",
    test2: 1
  }
  const testThing2 = {
    name: "blah2",
    test: "method test: blah blah",
    test2: 1
  }
  const testThing3 = {
    name: "blah3",
    test: "class test: blah blah",
    test2: 2
  }
  const updatedTestThing = {
    name: "blah",
    test: "method test: abc",
    test2: 22
  }

  expect(await db.g('blah')).toBeFalsy()

  await db.c(testThing)
  expect(await db.g('blah')).toEqual(testThing)

  await db.c(testThing2)
  await db.c(testThing3)
  const list = await db.gl()
  expect((<any>list).length).toBeDefined()
  expect((<any>list).length).toEqual(3)
  expect(list).toEqual(expect.arrayContaining([testThing, testThing2, testThing3]))

  const list2 = await db.gl2(1)
  expect((<any>list2).length).toBeDefined()
  expect((<any>list2).length).toEqual(2)
  expect(list2).toEqual(expect.arrayContaining([testThing, testThing2]))

  const list3 = await db.gwq({name: "blah2"})
  expect(list3).toBeDefined()
  expect(list3.length).toEqual(1)
  expect(list3).toEqual(expect.arrayContaining([testThing2]))

  await db.u('blah', updatedTestThing)
  expect(await db.g('blah')).toEqual(expect.objectContaining(updatedTestThing))

  await db.d('blah')
  expect(await db.g('blah')).toBeFalsy()
})

@database()
class XxYyDatabase {
  @dbGet() g(_id:string) {throw new Error(`called dbGet original method`)}
           getXxYyList() {}
           createXxYy(_thing:any) {throw new Error(`called dbCreate original method`)}
           updateXxYy(_name:string, _thing:any) {}
           deleteXxYy(_name:string) {}
}

test("Class and method decorators can be mixed to generate CRUD.", async () => {
  const db = new XxYyDatabase()

  const testThing = {
    name: "blah",
    test: "method test: blah blah",
    test2: 1
  }
  const testThing2 = {
    name: "blah2",
    test: "method test: blah blah",
    test2: 1
  }
  const updatedTestThing = {
    name: "blah",
    test: "method test: abc",
    test2: 22
  }

  expect(await db.g('blah')).toBeFalsy()

  await db.createXxYy(testThing)
  expect(await db.g('blah')).toEqual(testThing)

  await db.createXxYy(testThing2)
  const list = await db.getXxYyList()
  expect((<any>list).length).toBeDefined()
  expect((<any>list).length).toEqual(2)
  expect(list).toEqual(expect.arrayContaining([testThing, testThing2]))

  await db.updateXxYy('blah', updatedTestThing)
  expect(await db.g('blah')).toEqual(expect.objectContaining(updatedTestThing))

  await db.deleteXxYy('blah')
  expect(await db.g('blah')).toBeFalsy()
})

class YDatabase {
  @dbGet('z', 'id', async (db, id, z) => {
    if(!z)
      return z
    return Object.assign({ v: 'abc' }, z)
  })
  g(_id:string):any {throw new Error(`called dbGet original method`)}

  @dbGetList('z', async (db, zs) => {
    return zs.map((z) => Object.assign({w: 'abc'}, z))
  })
  gl():any {throw new Error(`called dbGetList original method`)}

  @dbCreate('z', async (db, z) => {
    const z_copy = Object.assign({copy:true}, z)
    z_copy.id = 'copy-'+z.id
    z_copy._id = undefined
    await db.collection('z').insertOne(z_copy)
  })
  c(_thing:any) {}

  @dbUpdate('z', 'id', async (db, id, z) => {
    const z_copy = Object.assign({copy:true}, z)
    z_copy.id = 'copy2-'+id
    z_copy._id = undefined
    await db.collection('z').insertOne(z_copy)
  })
  u(_name:string, _thing:any) {}

  @dbDelete('z', 'id', async (db, id) => {
    await db.collection('z').insertOne({id: 'deleted-'+id})
  })
  d(_name:string) {}
}

test("Callback is called and can modify result.", async () => {
  const db = new YDatabase()

  const testThing = {
    id: '5',
    test: "method test: blah blah",
    test2: 1
  }

  const testThing2 = {
    id: "6",
    test: "method test: blah blah",
    test2: 1
  }

  await db.c(testThing)
  const readValue = await db.g('5')
  expect(readValue).toBeDefined()
  expect(readValue.v).toEqual('abc')
  expect(readValue.id).toEqual('5')

  const copy = await db.g('copy-5')
  expect(copy).toBeDefined()
  expect(copy.copy).toBeTruthy()
  expect(copy.id).toEqual('copy-5')

  await db.c(testThing2)
  const list = await db.gl()
  expect(list).toEqual(expect.arrayContaining([
    Object.assign({w: 'abc'}, testThing),
    Object.assign({w: 'abc'}, testThing2)
  ]))

  await db.u('5', {
    id: '5'
  })
  const readValue2 = await db.g('5')
  expect(readValue2).toBeDefined()
  expect(readValue2.test).toBeUndefined()
  expect(readValue2.test2).toBeUndefined()
  expect(readValue2.id).toEqual('5')

  const copy2 = await db.g('copy2-5')
  expect(copy2).toBeDefined()
  expect(copy2.copy).toBeTruthy()
  expect(copy2.id).toEqual('copy2-5')

  await db.d('5')
  expect(await db.g('5')).toBeFalsy()
  const deletedVal = await db.g('deleted-5')
  expect(deletedVal).toBeTruthy()
  expect(deletedVal.id).toEqual('deleted-5')
})
