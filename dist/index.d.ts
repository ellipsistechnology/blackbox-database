import { Db } from 'mongodb';
export declare function configureDatabase(config: any): void;
/**
 * Overwrites each of the target class's methods with database access logic.
 * Classs methods must match <verb><typeName>[List].
 * For example: getThing to get a single Thing, getThingList to get a list of things.
 * Verbs accepted: get, create, update, delete.
 * The body of each method that matches this pattern will be replaced with an
 * implementation that performs the corresponding operation.
 */
export declare function database(typeName?: string, identifier?: string): (target: Function) => void;
export declare function dbGetList(typeName?: string, callback?: (db: Db, val: any) => any): (target: any, _methodName: string, descriptor: PropertyDescriptor) => PropertyDescriptor;
export declare function dbGetListBy(propertyName: string, typeName?: string, callback?: (db: Db, val: any) => any): (target: any, _methodName: string, descriptor: PropertyDescriptor) => PropertyDescriptor;
export declare function dbGet(typeName?: string, identifier?: string, callback?: (db: Db, id: string, val: any) => any): (target: any, _methodName: string, descriptor: PropertyDescriptor) => PropertyDescriptor;
export declare function dbGetBy(propertyName: string, typeName?: string, callback?: (db: Db, propertyName: string, val: any) => any): (target: any, _methodName: string, descriptor: PropertyDescriptor) => PropertyDescriptor;
export declare function dbGetListWithQuery(typeName?: string, callback?: (db: Db, val: any) => any): (target: any, _methodName: string, descriptor: PropertyDescriptor) => PropertyDescriptor;
export declare function dbCreate(typeName?: string, callback?: (db: Db, val: any) => void): (target: any, _methodName: string, descriptor: PropertyDescriptor) => PropertyDescriptor;
export declare function dbUpdate(typeName?: string, identifier?: string, callback?: (db: Db, id: string, val: any) => void): (target: any, _methodName: string, descriptor: PropertyDescriptor) => PropertyDescriptor;
export declare function dbDelete(typeName?: string, identifier?: string, callback?: (db: Db, id: string) => void): (target: any, _methodName: string, descriptor: PropertyDescriptor) => PropertyDescriptor;
