"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var __1 = require("..");
var method1Value = 'method1 value';
beforeAll(function () {
    __1.configureDatabase({
        url: global.__MONGO_URI__,
        dbname: global.__MONGO_DB_NAME__
    });
});
var ThingDatabase = /** @class */ (function () {
    function ThingDatabase() {
    }
    ThingDatabase.prototype.method1 = function () {
        return method1Value;
    };
    ThingDatabase.prototype.getThing = function (_id) {
        throw new Error("This should never be called.");
    };
    ThingDatabase.prototype.getThingList = function () { };
    ThingDatabase.prototype.createThing = function (_thing) { };
    ThingDatabase.prototype.updateThing = function (_name, _thing) { };
    ThingDatabase.prototype.deleteThing = function (_name) { };
    ThingDatabase.prototype.getThingByTest2 = function (_testValue) { };
    ThingDatabase.prototype.getThingListByTest2 = function (_testValue) { throw new Error('getThingListByTest2() placeholder called'); };
    ThingDatabase.prototype.getThingListWithQuery = function (_q) { return []; };
    ThingDatabase = __decorate([
        __1.database()
    ], ThingDatabase);
    return ThingDatabase;
}());
test("Class decorator generated CRUD", function () { return __awaiter(_this, void 0, void 0, function () {
    var db, testThing, testThing2, testThing3, updatedTestThing, _a, _b, _c, list, list2, _d, list3, _e, _f;
    return __generator(this, function (_g) {
        switch (_g.label) {
            case 0:
                db = new ThingDatabase();
                testThing = {
                    name: "blah",
                    test: "class test: blah blah",
                    test2: 1
                };
                testThing2 = {
                    name: "blah2",
                    test: "class test: blah blah",
                    test2: 1
                };
                testThing3 = {
                    name: "blah3",
                    test: "class test: blah blah",
                    test2: 2
                };
                updatedTestThing = {
                    name: "blah",
                    test: "class test: abc",
                    test2: 22
                };
                _a = expect;
                return [4 /*yield*/, db.method1()];
            case 1:
                _a.apply(void 0, [_g.sent()]).toEqual(method1Value);
                _b = expect;
                return [4 /*yield*/, db.getThing('blah')];
            case 2:
                _b.apply(void 0, [_g.sent()]).toBeFalsy();
                return [4 /*yield*/, db.createThing(testThing)];
            case 3:
                _g.sent();
                _c = expect;
                return [4 /*yield*/, db.getThing('blah')];
            case 4:
                _c.apply(void 0, [_g.sent()]).toEqual(testThing);
                return [4 /*yield*/, db.createThing(testThing2)];
            case 5:
                _g.sent();
                return [4 /*yield*/, db.createThing(testThing3)];
            case 6:
                _g.sent();
                return [4 /*yield*/, db.getThingList()];
            case 7:
                list = _g.sent();
                expect(list.length).toBeDefined();
                expect(list.length).toEqual(3);
                expect(list).toEqual(expect.arrayContaining([testThing, testThing2, testThing3]));
                return [4 /*yield*/, db.getThingListByTest2(1)];
            case 8:
                list2 = _g.sent();
                expect(list2.length).toBeDefined();
                expect(list2.length).toEqual(2);
                expect(list2).toEqual(expect.arrayContaining([testThing, testThing2]));
                _d = expect;
                return [4 /*yield*/, db.getThingByTest2(2)];
            case 9:
                _d.apply(void 0, [_g.sent()]).toEqual(testThing3);
                return [4 /*yield*/, db.getThingListWithQuery({ name: "blah2" })];
            case 10:
                list3 = _g.sent();
                expect(list3).toBeDefined();
                expect(list3.length).toEqual(1);
                expect(list3).toEqual(expect.arrayContaining([testThing2]));
                return [4 /*yield*/, db.updateThing('blah', updatedTestThing)];
            case 11:
                _g.sent();
                _e = expect;
                return [4 /*yield*/, db.getThing('blah')];
            case 12:
                _e.apply(void 0, [_g.sent()]).toEqual(expect.objectContaining(updatedTestThing));
                return [4 /*yield*/, db.deleteThing('blah')];
            case 13:
                _g.sent();
                _f = expect;
                return [4 /*yield*/, db.getThing('blah')];
            case 14:
                _f.apply(void 0, [_g.sent()]).toBeFalsy();
                return [2 /*return*/];
        }
    });
}); });
var BlahDatabase = /** @class */ (function () {
    function BlahDatabase() {
    }
    BlahDatabase.prototype.g = function (_id) { throw new Error("called dbGet original method"); };
    BlahDatabase.prototype.g2 = function (_val) { throw new Error("called dbGet original method"); };
    BlahDatabase.prototype.gl = function () { };
    BlahDatabase.prototype.gl2 = function (_val) { };
    BlahDatabase.prototype.c = function (_thing) { throw new Error("called dbCreate original method"); };
    BlahDatabase.prototype.u = function (_name, _thing) { };
    BlahDatabase.prototype.d = function (_name) { };
    BlahDatabase.prototype.gwq = function (_q) { return []; };
    __decorate([
        __1.dbGet()
    ], BlahDatabase.prototype, "g", null);
    __decorate([
        __1.dbGetBy('test2')
    ], BlahDatabase.prototype, "g2", null);
    __decorate([
        __1.dbGetList()
    ], BlahDatabase.prototype, "gl", null);
    __decorate([
        __1.dbGetListBy('test2')
    ], BlahDatabase.prototype, "gl2", null);
    __decorate([
        __1.dbCreate()
    ], BlahDatabase.prototype, "c", null);
    __decorate([
        __1.dbUpdate()
    ], BlahDatabase.prototype, "u", null);
    __decorate([
        __1.dbDelete()
    ], BlahDatabase.prototype, "d", null);
    __decorate([
        __1.dbGetListWithQuery()
    ], BlahDatabase.prototype, "gwq", null);
    return BlahDatabase;
}());
test("Method decorator generated CRUD.", function () { return __awaiter(_this, void 0, void 0, function () {
    var db, testThing, testThing2, testThing3, updatedTestThing, _a, _b, list, list2, list3, _c, _d;
    return __generator(this, function (_e) {
        switch (_e.label) {
            case 0:
                db = new BlahDatabase();
                testThing = {
                    name: "blah",
                    test: "method test: blah blah",
                    test2: 1
                };
                testThing2 = {
                    name: "blah2",
                    test: "method test: blah blah",
                    test2: 1
                };
                testThing3 = {
                    name: "blah3",
                    test: "class test: blah blah",
                    test2: 2
                };
                updatedTestThing = {
                    name: "blah",
                    test: "method test: abc",
                    test2: 22
                };
                _a = expect;
                return [4 /*yield*/, db.g('blah')];
            case 1:
                _a.apply(void 0, [_e.sent()]).toBeFalsy();
                return [4 /*yield*/, db.c(testThing)];
            case 2:
                _e.sent();
                _b = expect;
                return [4 /*yield*/, db.g('blah')];
            case 3:
                _b.apply(void 0, [_e.sent()]).toEqual(testThing);
                return [4 /*yield*/, db.c(testThing2)];
            case 4:
                _e.sent();
                return [4 /*yield*/, db.c(testThing3)];
            case 5:
                _e.sent();
                return [4 /*yield*/, db.gl()];
            case 6:
                list = _e.sent();
                expect(list.length).toBeDefined();
                expect(list.length).toEqual(3);
                expect(list).toEqual(expect.arrayContaining([testThing, testThing2, testThing3]));
                return [4 /*yield*/, db.gl2(1)];
            case 7:
                list2 = _e.sent();
                expect(list2.length).toBeDefined();
                expect(list2.length).toEqual(2);
                expect(list2).toEqual(expect.arrayContaining([testThing, testThing2]));
                return [4 /*yield*/, db.gwq({ name: "blah2" })];
            case 8:
                list3 = _e.sent();
                expect(list3).toBeDefined();
                expect(list3.length).toEqual(1);
                expect(list3).toEqual(expect.arrayContaining([testThing2]));
                return [4 /*yield*/, db.u('blah', updatedTestThing)];
            case 9:
                _e.sent();
                _c = expect;
                return [4 /*yield*/, db.g('blah')];
            case 10:
                _c.apply(void 0, [_e.sent()]).toEqual(expect.objectContaining(updatedTestThing));
                return [4 /*yield*/, db.d('blah')];
            case 11:
                _e.sent();
                _d = expect;
                return [4 /*yield*/, db.g('blah')];
            case 12:
                _d.apply(void 0, [_e.sent()]).toBeFalsy();
                return [2 /*return*/];
        }
    });
}); });
var XxYyDatabase = /** @class */ (function () {
    function XxYyDatabase() {
    }
    XxYyDatabase.prototype.g = function (_id) { throw new Error("called dbGet original method"); };
    XxYyDatabase.prototype.getXxYyList = function () { };
    XxYyDatabase.prototype.createXxYy = function (_thing) { throw new Error("called dbCreate original method"); };
    XxYyDatabase.prototype.updateXxYy = function (_name, _thing) { };
    XxYyDatabase.prototype.deleteXxYy = function (_name) { };
    __decorate([
        __1.dbGet()
    ], XxYyDatabase.prototype, "g", null);
    XxYyDatabase = __decorate([
        __1.database()
    ], XxYyDatabase);
    return XxYyDatabase;
}());
test("Class and method decorators can be mixed to generate CRUD.", function () { return __awaiter(_this, void 0, void 0, function () {
    var db, testThing, testThing2, updatedTestThing, _a, _b, list, _c, _d;
    return __generator(this, function (_e) {
        switch (_e.label) {
            case 0:
                db = new XxYyDatabase();
                testThing = {
                    name: "blah",
                    test: "method test: blah blah",
                    test2: 1
                };
                testThing2 = {
                    name: "blah2",
                    test: "method test: blah blah",
                    test2: 1
                };
                updatedTestThing = {
                    name: "blah",
                    test: "method test: abc",
                    test2: 22
                };
                _a = expect;
                return [4 /*yield*/, db.g('blah')];
            case 1:
                _a.apply(void 0, [_e.sent()]).toBeFalsy();
                return [4 /*yield*/, db.createXxYy(testThing)];
            case 2:
                _e.sent();
                _b = expect;
                return [4 /*yield*/, db.g('blah')];
            case 3:
                _b.apply(void 0, [_e.sent()]).toEqual(testThing);
                return [4 /*yield*/, db.createXxYy(testThing2)];
            case 4:
                _e.sent();
                return [4 /*yield*/, db.getXxYyList()];
            case 5:
                list = _e.sent();
                expect(list.length).toBeDefined();
                expect(list.length).toEqual(2);
                expect(list).toEqual(expect.arrayContaining([testThing, testThing2]));
                return [4 /*yield*/, db.updateXxYy('blah', updatedTestThing)];
            case 6:
                _e.sent();
                _c = expect;
                return [4 /*yield*/, db.g('blah')];
            case 7:
                _c.apply(void 0, [_e.sent()]).toEqual(expect.objectContaining(updatedTestThing));
                return [4 /*yield*/, db.deleteXxYy('blah')];
            case 8:
                _e.sent();
                _d = expect;
                return [4 /*yield*/, db.g('blah')];
            case 9:
                _d.apply(void 0, [_e.sent()]).toBeFalsy();
                return [2 /*return*/];
        }
    });
}); });
var YDatabase = /** @class */ (function () {
    function YDatabase() {
    }
    YDatabase.prototype.g = function (_id) { throw new Error("called dbGet original method"); };
    YDatabase.prototype.gl = function () { throw new Error("called dbGetList original method"); };
    YDatabase.prototype.c = function (_thing) { };
    YDatabase.prototype.u = function (_name, _thing) { };
    YDatabase.prototype.d = function (_name) { };
    __decorate([
        __1.dbGet('z', 'id', function (db, id, z) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (!z)
                    return [2 /*return*/, z];
                return [2 /*return*/, Object.assign({ v: 'abc' }, z)];
            });
        }); })
    ], YDatabase.prototype, "g", null);
    __decorate([
        __1.dbGetList('z', function (db, zs) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, zs.map(function (z) { return Object.assign({ w: 'abc' }, z); })];
            });
        }); })
    ], YDatabase.prototype, "gl", null);
    __decorate([
        __1.dbCreate('z', function (db, z) { return __awaiter(_this, void 0, void 0, function () {
            var z_copy;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        z_copy = Object.assign({ copy: true }, z);
                        z_copy.id = 'copy-' + z.id;
                        z_copy._id = undefined;
                        return [4 /*yield*/, db.collection('z').insertOne(z_copy)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); })
    ], YDatabase.prototype, "c", null);
    __decorate([
        __1.dbUpdate('z', 'id', function (db, id, z) { return __awaiter(_this, void 0, void 0, function () {
            var z_copy;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        z_copy = Object.assign({ copy: true }, z);
                        z_copy.id = 'copy2-' + id;
                        z_copy._id = undefined;
                        return [4 /*yield*/, db.collection('z').insertOne(z_copy)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); })
    ], YDatabase.prototype, "u", null);
    __decorate([
        __1.dbDelete('z', 'id', function (db, id) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, db.collection('z').insertOne({ id: 'deleted-' + id })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); })
    ], YDatabase.prototype, "d", null);
    return YDatabase;
}());
test("Callback is called and can modify result.", function () { return __awaiter(_this, void 0, void 0, function () {
    var db, testThing, testThing2, readValue, copy, list, readValue2, copy2, _a, deletedVal;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                db = new YDatabase();
                testThing = {
                    id: '5',
                    test: "method test: blah blah",
                    test2: 1
                };
                testThing2 = {
                    id: "6",
                    test: "method test: blah blah",
                    test2: 1
                };
                return [4 /*yield*/, db.c(testThing)];
            case 1:
                _b.sent();
                return [4 /*yield*/, db.g('5')];
            case 2:
                readValue = _b.sent();
                expect(readValue).toBeDefined();
                expect(readValue.v).toEqual('abc');
                expect(readValue.id).toEqual('5');
                return [4 /*yield*/, db.g('copy-5')];
            case 3:
                copy = _b.sent();
                expect(copy).toBeDefined();
                expect(copy.copy).toBeTruthy();
                expect(copy.id).toEqual('copy-5');
                return [4 /*yield*/, db.c(testThing2)];
            case 4:
                _b.sent();
                return [4 /*yield*/, db.gl()];
            case 5:
                list = _b.sent();
                expect(list).toEqual(expect.arrayContaining([
                    Object.assign({ w: 'abc' }, testThing),
                    Object.assign({ w: 'abc' }, testThing2)
                ]));
                return [4 /*yield*/, db.u('5', {
                        id: '5'
                    })];
            case 6:
                _b.sent();
                return [4 /*yield*/, db.g('5')];
            case 7:
                readValue2 = _b.sent();
                expect(readValue2).toBeDefined();
                expect(readValue2.test).toBeUndefined();
                expect(readValue2.test2).toBeUndefined();
                expect(readValue2.id).toEqual('5');
                return [4 /*yield*/, db.g('copy2-5')];
            case 8:
                copy2 = _b.sent();
                expect(copy2).toBeDefined();
                expect(copy2.copy).toBeTruthy();
                expect(copy2.id).toEqual('copy2-5');
                return [4 /*yield*/, db.d('5')];
            case 9:
                _b.sent();
                _a = expect;
                return [4 /*yield*/, db.g('5')];
            case 10:
                _a.apply(void 0, [_b.sent()]).toBeFalsy();
                return [4 /*yield*/, db.g('deleted-5')];
            case 11:
                deletedVal = _b.sent();
                expect(deletedVal).toBeTruthy();
                expect(deletedVal.id).toEqual('deleted-5');
                return [2 /*return*/];
        }
    });
}); });
