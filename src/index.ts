import {MongoClient, Db} from 'mongodb'

let bbiocClient: BBIOCClient
export function configureDatabase(config) {
  bbiocClient = new BBIOCClient(config)
}

class BBIOCClient {
  config:any

  constructor(config) {
    this.config = config
    this.call = this.call.bind(this)
    this.connect = this.connect.bind(this)
  }

  private async connect() {
    if(!this.config)
      throw new Error('Database not configured: configureDatabase() must be called with a valid configuration prior to using the @database decoration.')

    const client = new MongoClient(this.config.url, {useNewUrlParser: true});
    await client.connect();
    const db = client.db(this.config.dbname);
    return { db, client };
  }

  async call(callback: (any: any) => any) {
    if(!this.config)
      throw new Error('Database not configured: configureDatabase() must be called with a valid configuration prior to using the @database decoration.')

    const { client, db } = await this.connect()
    let res = await callback(db)
    client.close()
    return res
  }
}

function makeDatabaseMethods(typeName:string, target:Function, identifier:string) {
  const methodMatcher = new RegExp(`^(get|create|update|delete)${typeName}(ListBy|ListWithQuery|List|By)?([A-Z][A-Za-z0-9_]*)?$`)
  Object.getOwnPropertyNames(target.prototype).filter((key:string) => typeof target.prototype[key] === 'function').forEach((methodName:string) => {
    const matches = methodMatcher.exec(methodName)
    if(matches && matches.length >= 3) {
      const verb = matches[1]
      let typeMod: string
      let property: string
      if(matches.length > 2 && matches[2]) {
        typeMod = matches[2]
      }
      if(matches.length > 3 && matches[3]) {
        property = matches[3].charAt(0).toLowerCase() + matches[3].substring(1)
      }

      const collectionName = typeName.toLowerCase()

      switch(verb) {
        case 'get':
        switch(typeMod) {
          case 'List':
            if(!target.prototype.__bbdb_dbGetList) {
              target.prototype.__bbdb_dbGetList = true
              target.prototype[methodName] = makeGetListMethod(collectionName);
            }
            break;
          case 'ListBy':
            if(!target.prototype['__bbdb_dbGetListBy'+property]) {
              target.prototype['__bbdb_dbGetListBy'+property] = true
              target.prototype[methodName] = makeGetListByMethod(property, collectionName);
            }
            break;
          case 'By':
            if(!target.prototype['__bbdb_dbGetBy'+property]) {
              target.prototype['__bbdb_dbGetBy'+property] = true
              target.prototype[methodName] = makeGetByMethod(property, collectionName);
            }
            break;
          case 'ListWithQuery':
            if(!target.prototype['__bbdb_dbGetListWithQuery']) {
              target.prototype['__bbdb_dbGetListWithQuery'] = true
              target.prototype[methodName] = makeGetListWithQueryMethod(collectionName);
            }
            break;
          default: // no typemod, e.g. getThing()
            if(!target.prototype.__bbdb_dbGet) {
              target.prototype.__bbdb_dbGet = true
              target.prototype[methodName] = makeGetMethod(identifier, collectionName);
            }
        }
        break;

        case 'create':
          if(!target.prototype.__bbdb_dbCreate) {
            target.prototype.__bbdb_dbCreate = true
            target.prototype[methodName] = makeCreateMethod(collectionName);
          }
        break;

        case 'update':
          if(!target.prototype.__bbdb_dbUpdate) {
            target.prototype.__bbdb_dbUpdate = true
            target.prototype[methodName] = makeUpdateMethod(identifier, collectionName);
          }
        break;

        case 'delete':
          if(!target.prototype.__bbdb_dbDelete) {
            target.prototype.__bbdb_dbDelete = true
            target.prototype[methodName] = makeDeleteMethod(identifier, collectionName);
          }
        break;
      }
    }
  })
}

function makeDeleteMethod(identifier: string, collectionName: string, callback: (db:Db, id:string) => void = undefined) {
  return async (id: string) => await bbiocClient.call(async (db: Db) => {
    const query = {};
    query[identifier] = id;
    await db.collection(collectionName).deleteOne(query);
    if(callback)
      await callback(db, id)
  });
}

function makeUpdateMethod(identifier: string, collectionName: string, callback: (db:Db, id:string, val:any) => void = undefined) {
  return async (id: string, entity: any) => await bbiocClient.call(async (db: Db) => {
    const query = {};
    query[identifier] = id;
    await db.collection(collectionName).replaceOne(query, entity);
    if(callback)
      await callback(db, id, entity)
  });
}

function makeCreateMethod(collectionName: string, callback: (db:Db, val:any) => void = undefined) {
  return async (entity: any) => await bbiocClient.call(async (db: Db) => {
    await db.collection(collectionName).insertOne(entity);
    if(callback)
      await callback(db, entity)
  });
}

function makeGetMethod(identifier: string, collectionName: string, callback: (db:Db, id:string, val:any) => any = undefined) {
  return async (id: string) => await bbiocClient.call(async (db: Db) => {
    const query = {};
    query[identifier] = id;
    let val = await db.collection(collectionName).findOne(query);
    if(callback)
      val = await callback(db, id, val)
    return val
  });
}

function makeGetByMethod(propertyName: string, collectionName: string, callback: (db:Db, propertyName:string, val:any) => any = undefined) {
  return async (propertyValue: any) => await bbiocClient.call(async (db: Db) => {
    const query = {};
    query[propertyName] = propertyValue;
    let val = await db.collection(collectionName).findOne(query);
    if(callback) {
      val = await callback(db, propertyName, val)
    }
    return val
  });
}

function makeGetListWithQueryMethod(collectionName: string, callback: (db:Db, val:any) => any = undefined) {
  return async (query: any) => await bbiocClient.call(async (db: Db) => {
    let val = db.collection(collectionName).find(query).toArray();
    if(callback) {
      val = await callback(db, val)
    }
    return val
  });
}

function makeGetListMethod(collectionName: string, callback: (db:Db, val:any) => any = undefined) {
  return async () => await bbiocClient.call(async (db: Db) => {
    let val = await db.collection(collectionName).find().toArray();
    if(callback)
      val = await callback(db, val)
    return val
  });
}

function makeGetListByMethod(propertyName: string, collectionName: string, callback: (db:Db, val:any) => any = undefined) {
  return async (propertyValue: any) => await bbiocClient.call(async (db: Db) => {
    const query = {}
    query[propertyName] = propertyValue
    let val = await db.collection(collectionName).find(query).toArray();
    if(callback)
      val = await callback(db, val)
    return val
  });
}

/**
 * Overwrites each of the target class's methods with database access logic.
 * Classs methods must match <verb><typeName>[List].
 * For example: getThing to get a single Thing, getThingList to get a list of things.
 * Verbs accepted: get, create, update, delete.
 * The body of each method that matches this pattern will be replaced with an
 * implementation that performs the corresponding operation.
 */
export function database(typeName:string = undefined, identifier:string = 'name') {
  return function(target: Function) {
    typeName = extractTypeName(typeName, target);
    makeDatabaseMethods(typeName, target, identifier)
  }
}

/**
 * Extracts the datatype name from the class name.
 */
function extractTypeName(typeName: string, target: Function) {
  if (!typeName) {
    let name = target.name ? target.name : target.constructor && target.constructor.name ? target.constructor.name : ''
    const match = name.match(/^[A-Z]\w*(?=Database$)/);
    if (!match)
      throw new Error(`@database specification error: typeName not provided and class name does not match <Type>Database: '${target.name}'`);
    typeName = match[0]
  }
  return typeName;
}

export function dbGetList(typeName:string = undefined, callback: (db:Db, val:any) => any = undefined) {
  return function(target: any, _methodName: string, descriptor: PropertyDescriptor) {
    target.__bbdb_dbGetList = true
    typeName = extractTypeName(typeName, target);
    descriptor.value = makeGetListMethod(typeName.toLowerCase(), callback)
    return descriptor
  }
}

export function dbGetListBy(propertyName: string, typeName:string = undefined, callback: (db:Db, val:any) => any = undefined) {
  return function(target: any, _methodName: string, descriptor: PropertyDescriptor) {
    target['__bbdb_dbGetListBy'+propertyName] = true
    typeName = extractTypeName(typeName, target);
    descriptor.value = makeGetListByMethod(propertyName, typeName.toLowerCase(), callback)
    return descriptor
  }
}

export function dbGet(typeName:string = undefined, identifier:string = 'name', callback: (db:Db, id:string, val:any) => any = undefined) {
  return function(target: any, _methodName: string, descriptor: PropertyDescriptor) {
    target.__bbdb_dbGet = true
    typeName = extractTypeName(typeName, target);
    descriptor.value = makeGetMethod(identifier, typeName.toLowerCase(), callback)
    return descriptor
  }
}

export function dbGetBy(propertyName:string, typeName:string = undefined, callback: (db:Db, propertyName:string, val:any) => any = undefined) {
  return function(target: any, _methodName: string, descriptor: PropertyDescriptor) {
    target['__bbdb_dbGetBy'+propertyName] = true
    typeName = extractTypeName(typeName, target);
    descriptor.value = makeGetByMethod(propertyName, typeName.toLowerCase(), callback)
    return descriptor
  }
}

export function dbGetListWithQuery(typeName:string = undefined, callback: (db:Db, val:any) => any = undefined) {
  return function(target: any, _methodName: string, descriptor: PropertyDescriptor) {
    target['__bbdb_dbGetListWithQuery'] = true
    typeName = extractTypeName(typeName, target);
    descriptor.value = makeGetListWithQueryMethod(typeName.toLowerCase(), callback)
    return descriptor
  }
}

export function dbCreate(typeName:string = undefined, callback: (db:Db, val:any) => void = undefined) {
  return function(target: any, _methodName: string, descriptor: PropertyDescriptor) {
    target.__bbdb_dbCreate = true
    typeName = extractTypeName(typeName, target);
    descriptor.value = makeCreateMethod(typeName.toLowerCase(), callback)
    return descriptor
  }
}

export function dbUpdate(typeName:string = undefined, identifier:string = 'name', callback: (db:Db, id:string, val:any) => void = undefined) {
  return function(target: any, _methodName: string, descriptor: PropertyDescriptor) {
    target.__bbdb_dbUpdate = true
    typeName = extractTypeName(typeName, target);
    descriptor.value = makeUpdateMethod(identifier, typeName.toLowerCase(), callback)
    return descriptor
  }
}

export function dbDelete(typeName:string = undefined, identifier:string = 'name', callback: (db:Db, id:string) => void = undefined) {
  return function(target: any, _methodName: string, descriptor: PropertyDescriptor) {
    target.__bbdb_dbDelete = true
    typeName = extractTypeName(typeName, target);
    descriptor.value = makeDeleteMethod(identifier, typeName.toLowerCase(), callback)
    return descriptor
  }
}
