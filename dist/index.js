"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongodb_1 = require("mongodb");
var bbiocClient;
function configureDatabase(config) {
    bbiocClient = new BBIOCClient(config);
}
exports.configureDatabase = configureDatabase;
var BBIOCClient = /** @class */ (function () {
    function BBIOCClient(config) {
        this.config = config;
        this.call = this.call.bind(this);
        this.connect = this.connect.bind(this);
    }
    BBIOCClient.prototype.connect = function () {
        return __awaiter(this, void 0, void 0, function () {
            var client, db;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.config)
                            throw new Error('Database not configured: configureDatabase() must be called with a valid configuration prior to using the @database decoration.');
                        client = new mongodb_1.MongoClient(this.config.url, { useNewUrlParser: true });
                        return [4 /*yield*/, client.connect()];
                    case 1:
                        _a.sent();
                        db = client.db(this.config.dbname);
                        return [2 /*return*/, { db: db, client: client }];
                }
            });
        });
    };
    BBIOCClient.prototype.call = function (callback) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, client, db, res;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (!this.config)
                            throw new Error('Database not configured: configureDatabase() must be called with a valid configuration prior to using the @database decoration.');
                        return [4 /*yield*/, this.connect()];
                    case 1:
                        _a = _b.sent(), client = _a.client, db = _a.db;
                        return [4 /*yield*/, callback(db)];
                    case 2:
                        res = _b.sent();
                        client.close();
                        return [2 /*return*/, res];
                }
            });
        });
    };
    return BBIOCClient;
}());
function makeDatabaseMethods(typeName, target, identifier) {
    var methodMatcher = new RegExp("^(get|create|update|delete)" + typeName + "(ListBy|ListWithQuery|List|By)?([A-Z][A-Za-z0-9_]*)?$");
    Object.getOwnPropertyNames(target.prototype).filter(function (key) { return typeof target.prototype[key] === 'function'; }).forEach(function (methodName) {
        var matches = methodMatcher.exec(methodName);
        if (matches && matches.length >= 3) {
            var verb = matches[1];
            var typeMod = void 0;
            var property = void 0;
            if (matches.length > 2 && matches[2]) {
                typeMod = matches[2];
            }
            if (matches.length > 3 && matches[3]) {
                property = matches[3].charAt(0).toLowerCase() + matches[3].substring(1);
            }
            var collectionName = typeName.toLowerCase();
            switch (verb) {
                case 'get':
                    switch (typeMod) {
                        case 'List':
                            if (!target.prototype.__bbdb_dbGetList) {
                                target.prototype.__bbdb_dbGetList = true;
                                target.prototype[methodName] = makeGetListMethod(collectionName);
                            }
                            break;
                        case 'ListBy':
                            if (!target.prototype['__bbdb_dbGetListBy' + property]) {
                                target.prototype['__bbdb_dbGetListBy' + property] = true;
                                target.prototype[methodName] = makeGetListByMethod(property, collectionName);
                            }
                            break;
                        case 'By':
                            if (!target.prototype['__bbdb_dbGetBy' + property]) {
                                target.prototype['__bbdb_dbGetBy' + property] = true;
                                target.prototype[methodName] = makeGetByMethod(property, collectionName);
                            }
                            break;
                        case 'ListWithQuery':
                            if (!target.prototype['__bbdb_dbGetListWithQuery']) {
                                target.prototype['__bbdb_dbGetListWithQuery'] = true;
                                target.prototype[methodName] = makeGetListWithQueryMethod(collectionName);
                            }
                            break;
                        default: // no typemod, e.g. getThing()
                            if (!target.prototype.__bbdb_dbGet) {
                                target.prototype.__bbdb_dbGet = true;
                                target.prototype[methodName] = makeGetMethod(identifier, collectionName);
                            }
                    }
                    break;
                case 'create':
                    if (!target.prototype.__bbdb_dbCreate) {
                        target.prototype.__bbdb_dbCreate = true;
                        target.prototype[methodName] = makeCreateMethod(collectionName);
                    }
                    break;
                case 'update':
                    if (!target.prototype.__bbdb_dbUpdate) {
                        target.prototype.__bbdb_dbUpdate = true;
                        target.prototype[methodName] = makeUpdateMethod(identifier, collectionName);
                    }
                    break;
                case 'delete':
                    if (!target.prototype.__bbdb_dbDelete) {
                        target.prototype.__bbdb_dbDelete = true;
                        target.prototype[methodName] = makeDeleteMethod(identifier, collectionName);
                    }
                    break;
            }
        }
    });
}
function makeDeleteMethod(identifier, collectionName, callback) {
    var _this = this;
    if (callback === void 0) { callback = undefined; }
    return function (id) { return __awaiter(_this, void 0, void 0, function () {
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, bbiocClient.call(function (db) { return __awaiter(_this, void 0, void 0, function () {
                        var query;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    query = {};
                                    query[identifier] = id;
                                    return [4 /*yield*/, db.collection(collectionName).deleteOne(query)];
                                case 1:
                                    _a.sent();
                                    if (!callback) return [3 /*break*/, 3];
                                    return [4 /*yield*/, callback(db, id)];
                                case 2:
                                    _a.sent();
                                    _a.label = 3;
                                case 3: return [2 /*return*/];
                            }
                        });
                    }); })];
                case 1: return [2 /*return*/, _a.sent()];
            }
        });
    }); };
}
function makeUpdateMethod(identifier, collectionName, callback) {
    var _this = this;
    if (callback === void 0) { callback = undefined; }
    return function (id, entity) { return __awaiter(_this, void 0, void 0, function () {
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, bbiocClient.call(function (db) { return __awaiter(_this, void 0, void 0, function () {
                        var query;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    query = {};
                                    query[identifier] = id;
                                    return [4 /*yield*/, db.collection(collectionName).replaceOne(query, entity)];
                                case 1:
                                    _a.sent();
                                    if (!callback) return [3 /*break*/, 3];
                                    return [4 /*yield*/, callback(db, id, entity)];
                                case 2:
                                    _a.sent();
                                    _a.label = 3;
                                case 3: return [2 /*return*/];
                            }
                        });
                    }); })];
                case 1: return [2 /*return*/, _a.sent()];
            }
        });
    }); };
}
function makeCreateMethod(collectionName, callback) {
    var _this = this;
    if (callback === void 0) { callback = undefined; }
    return function (entity) { return __awaiter(_this, void 0, void 0, function () {
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, bbiocClient.call(function (db) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, db.collection(collectionName).insertOne(entity)];
                                case 1:
                                    _a.sent();
                                    if (!callback) return [3 /*break*/, 3];
                                    return [4 /*yield*/, callback(db, entity)];
                                case 2:
                                    _a.sent();
                                    _a.label = 3;
                                case 3: return [2 /*return*/];
                            }
                        });
                    }); })];
                case 1: return [2 /*return*/, _a.sent()];
            }
        });
    }); };
}
function makeGetMethod(identifier, collectionName, callback) {
    var _this = this;
    if (callback === void 0) { callback = undefined; }
    return function (id) { return __awaiter(_this, void 0, void 0, function () {
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, bbiocClient.call(function (db) { return __awaiter(_this, void 0, void 0, function () {
                        var query, val;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    query = {};
                                    query[identifier] = id;
                                    return [4 /*yield*/, db.collection(collectionName).findOne(query)];
                                case 1:
                                    val = _a.sent();
                                    if (!callback) return [3 /*break*/, 3];
                                    return [4 /*yield*/, callback(db, id, val)];
                                case 2:
                                    val = _a.sent();
                                    _a.label = 3;
                                case 3: return [2 /*return*/, val];
                            }
                        });
                    }); })];
                case 1: return [2 /*return*/, _a.sent()];
            }
        });
    }); };
}
function makeGetByMethod(propertyName, collectionName, callback) {
    var _this = this;
    if (callback === void 0) { callback = undefined; }
    return function (propertyValue) { return __awaiter(_this, void 0, void 0, function () {
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, bbiocClient.call(function (db) { return __awaiter(_this, void 0, void 0, function () {
                        var query, val;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    query = {};
                                    query[propertyName] = propertyValue;
                                    return [4 /*yield*/, db.collection(collectionName).findOne(query)];
                                case 1:
                                    val = _a.sent();
                                    if (!callback) return [3 /*break*/, 3];
                                    return [4 /*yield*/, callback(db, propertyName, val)];
                                case 2:
                                    val = _a.sent();
                                    _a.label = 3;
                                case 3: return [2 /*return*/, val];
                            }
                        });
                    }); })];
                case 1: return [2 /*return*/, _a.sent()];
            }
        });
    }); };
}
function makeGetListWithQueryMethod(collectionName, callback) {
    var _this = this;
    if (callback === void 0) { callback = undefined; }
    return function (query) { return __awaiter(_this, void 0, void 0, function () {
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, bbiocClient.call(function (db) { return __awaiter(_this, void 0, void 0, function () {
                        var val;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    val = db.collection(collectionName).find(query).toArray();
                                    if (!callback) return [3 /*break*/, 2];
                                    return [4 /*yield*/, callback(db, val)];
                                case 1:
                                    val = _a.sent();
                                    _a.label = 2;
                                case 2: return [2 /*return*/, val];
                            }
                        });
                    }); })];
                case 1: return [2 /*return*/, _a.sent()];
            }
        });
    }); };
}
function makeGetListMethod(collectionName, callback) {
    var _this = this;
    if (callback === void 0) { callback = undefined; }
    return function () { return __awaiter(_this, void 0, void 0, function () {
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, bbiocClient.call(function (db) { return __awaiter(_this, void 0, void 0, function () {
                        var val;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, db.collection(collectionName).find().toArray()];
                                case 1:
                                    val = _a.sent();
                                    if (!callback) return [3 /*break*/, 3];
                                    return [4 /*yield*/, callback(db, val)];
                                case 2:
                                    val = _a.sent();
                                    _a.label = 3;
                                case 3: return [2 /*return*/, val];
                            }
                        });
                    }); })];
                case 1: return [2 /*return*/, _a.sent()];
            }
        });
    }); };
}
function makeGetListByMethod(propertyName, collectionName, callback) {
    var _this = this;
    if (callback === void 0) { callback = undefined; }
    return function (propertyValue) { return __awaiter(_this, void 0, void 0, function () {
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, bbiocClient.call(function (db) { return __awaiter(_this, void 0, void 0, function () {
                        var query, val;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    query = {};
                                    query[propertyName] = propertyValue;
                                    return [4 /*yield*/, db.collection(collectionName).find(query).toArray()];
                                case 1:
                                    val = _a.sent();
                                    if (!callback) return [3 /*break*/, 3];
                                    return [4 /*yield*/, callback(db, val)];
                                case 2:
                                    val = _a.sent();
                                    _a.label = 3;
                                case 3: return [2 /*return*/, val];
                            }
                        });
                    }); })];
                case 1: return [2 /*return*/, _a.sent()];
            }
        });
    }); };
}
/**
 * Overwrites each of the target class's methods with database access logic.
 * Classs methods must match <verb><typeName>[List].
 * For example: getThing to get a single Thing, getThingList to get a list of things.
 * Verbs accepted: get, create, update, delete.
 * The body of each method that matches this pattern will be replaced with an
 * implementation that performs the corresponding operation.
 */
function database(typeName, identifier) {
    if (typeName === void 0) { typeName = undefined; }
    if (identifier === void 0) { identifier = 'name'; }
    return function (target) {
        typeName = extractTypeName(typeName, target);
        makeDatabaseMethods(typeName, target, identifier);
    };
}
exports.database = database;
/**
 * Extracts the datatype name from the class name.
 */
function extractTypeName(typeName, target) {
    if (!typeName) {
        var name_1 = target.name ? target.name : target.constructor && target.constructor.name ? target.constructor.name : '';
        var match = name_1.match(/^[A-Z]\w*(?=Database$)/);
        if (!match)
            throw new Error("@database specification error: typeName not provided and class name does not match <Type>Database: '" + target.name + "'");
        typeName = match[0];
    }
    return typeName;
}
function dbGetList(typeName, callback) {
    if (typeName === void 0) { typeName = undefined; }
    if (callback === void 0) { callback = undefined; }
    return function (target, _methodName, descriptor) {
        target.__bbdb_dbGetList = true;
        typeName = extractTypeName(typeName, target);
        descriptor.value = makeGetListMethod(typeName.toLowerCase(), callback);
        return descriptor;
    };
}
exports.dbGetList = dbGetList;
function dbGetListBy(propertyName, typeName, callback) {
    if (typeName === void 0) { typeName = undefined; }
    if (callback === void 0) { callback = undefined; }
    return function (target, _methodName, descriptor) {
        target['__bbdb_dbGetListBy' + propertyName] = true;
        typeName = extractTypeName(typeName, target);
        descriptor.value = makeGetListByMethod(propertyName, typeName.toLowerCase(), callback);
        return descriptor;
    };
}
exports.dbGetListBy = dbGetListBy;
function dbGet(typeName, identifier, callback) {
    if (typeName === void 0) { typeName = undefined; }
    if (identifier === void 0) { identifier = 'name'; }
    if (callback === void 0) { callback = undefined; }
    return function (target, _methodName, descriptor) {
        target.__bbdb_dbGet = true;
        typeName = extractTypeName(typeName, target);
        descriptor.value = makeGetMethod(identifier, typeName.toLowerCase(), callback);
        return descriptor;
    };
}
exports.dbGet = dbGet;
function dbGetBy(propertyName, typeName, callback) {
    if (typeName === void 0) { typeName = undefined; }
    if (callback === void 0) { callback = undefined; }
    return function (target, _methodName, descriptor) {
        target['__bbdb_dbGetBy' + propertyName] = true;
        typeName = extractTypeName(typeName, target);
        descriptor.value = makeGetByMethod(propertyName, typeName.toLowerCase(), callback);
        return descriptor;
    };
}
exports.dbGetBy = dbGetBy;
function dbGetListWithQuery(typeName, callback) {
    if (typeName === void 0) { typeName = undefined; }
    if (callback === void 0) { callback = undefined; }
    return function (target, _methodName, descriptor) {
        target['__bbdb_dbGetListWithQuery'] = true;
        typeName = extractTypeName(typeName, target);
        descriptor.value = makeGetListWithQueryMethod(typeName.toLowerCase(), callback);
        return descriptor;
    };
}
exports.dbGetListWithQuery = dbGetListWithQuery;
function dbCreate(typeName, callback) {
    if (typeName === void 0) { typeName = undefined; }
    if (callback === void 0) { callback = undefined; }
    return function (target, _methodName, descriptor) {
        target.__bbdb_dbCreate = true;
        typeName = extractTypeName(typeName, target);
        descriptor.value = makeCreateMethod(typeName.toLowerCase(), callback);
        return descriptor;
    };
}
exports.dbCreate = dbCreate;
function dbUpdate(typeName, identifier, callback) {
    if (typeName === void 0) { typeName = undefined; }
    if (identifier === void 0) { identifier = 'name'; }
    if (callback === void 0) { callback = undefined; }
    return function (target, _methodName, descriptor) {
        target.__bbdb_dbUpdate = true;
        typeName = extractTypeName(typeName, target);
        descriptor.value = makeUpdateMethod(identifier, typeName.toLowerCase(), callback);
        return descriptor;
    };
}
exports.dbUpdate = dbUpdate;
function dbDelete(typeName, identifier, callback) {
    if (typeName === void 0) { typeName = undefined; }
    if (identifier === void 0) { identifier = 'name'; }
    if (callback === void 0) { callback = undefined; }
    return function (target, _methodName, descriptor) {
        target.__bbdb_dbDelete = true;
        typeName = extractTypeName(typeName, target);
        descriptor.value = makeDeleteMethod(identifier, typeName.toLowerCase(), callback);
        return descriptor;
    };
}
exports.dbDelete = dbDelete;
